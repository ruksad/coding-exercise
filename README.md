# Roulette-prototype

Considering Player names are case insensitive

For designing the roulette run I have used spring boot shell,
1. Roulette game properties are configurable given in application.yml
2. To load players data from file is configurable one default file is given in classpath resources
3. To run the application use following commands
          1. start: to start the game
          2. stop: to stop the game
4. Application does not display anything until it gets an input

For this application I have used
1. ArrayBlocking queue so that user input in not piled more than the capacity of arrayBlocking queue within next move of
ScheduledExecutorService
2. ExecutorService is scheduled with fixed delay which is configurable , and due to this placing bet and choosing random number is concurrent.
3. To generate random number i have used ThreadLocalRandom because it gives performance over Random


There could me more enhancements like
1. Graceful shut down of application
2. More user friendly message to
3. and Results could be saved back to csv file
4. Needs more unit test cases