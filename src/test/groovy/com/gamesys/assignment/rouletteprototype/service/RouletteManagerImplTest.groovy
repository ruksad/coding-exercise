package com.gamesys.assignment.rouletteprototype.service

import com.gamesys.assignment.rouletteprototype.model.CurrentBet
import com.gamesys.assignment.rouletteprototype.model.Player
import com.gamesys.assignment.rouletteprototype.model.RandomSpinner
import org.springframework.core.io.ClassPathResource
import spock.lang.Specification
import uk.co.jemos.podam.api.PodamFactoryImpl

import java.util.concurrent.ScheduledExecutorService

class RouletteManagerImplTest extends Specification {
    def schedulerService = Mock(ScheduledExecutorService)
    def randomSpinner = Mock(RandomSpinner)
    def consoleService = Mock(ConsoleServiceImpl)
    ClassPathResource loadStoreFile = new ClassPathResource("store.csv")
    def spinnerTime = 30l
    def evenReward = 2;
    def oddReward = 3;
    def numberReward = 36;

    def rouletteManagerImpl;
    def podamFactory=new PodamFactoryImpl();
    def playStore=new HashMap()

    void setup() {
        def playerList = podamFactory.manufacturePojo(ArrayList.class,Player.class)
        ((Player)playerList.get(0)).setPlayerName("Ruksad");
        ((Player)playerList.get(1)).setPlayerName("Siddiqui");
        ((Player)playerList.get(2)).setPlayerName("Barbara");

        for(Player player:playerList){
            playStore.put(player.getPlayerName(),player)
        }

        rouletteManagerImpl = new RouletteManagerImpl(loadStoreFile.getFile().getPath(),
                spinnerTime, evenReward, oddReward, numberReward, schedulerService, randomSpinner, consoleService)
        rouletteManagerImpl.setPlayersStore(playStore)
    }

    def "LoadStore"() {
        given:" a file path load players"

        when:
        def res = ((RouletteManagerImpl) rouletteManagerImpl).loadStore()
        then:
        res
    }

    def "evaluateBetForPlayer"() {
        given:" a bet "
        def bet=podamFactory.manufacturePojo(CurrentBet.class)
        def totalBet=((CurrentBet)bet).getPlayer().getTotalBet()
        when:
        def currentBet = ((RouletteManagerImpl) rouletteManagerImpl).evaluateBetForPlayer(bet,new Random().nextInt())
        then:
        ((CurrentBet)currentBet).getPlayer().getTotalBet().compareTo(totalBet)>=0
    }

    def "rewardPlayersForBetAndUpdatePalerInfo when bet playerBetTypeinfo and random number is same"(){
        given:"a bet "
        def bet=podamFactory.manufacturePojo(CurrentBet.class)
        ((CurrentBet)bet).setCurrentBetType(CurrentBet.CurrentBetType.NUMBER)
        ((CurrentBet)bet).setPlayerBetTypeInfo("10")
        ((CurrentBet)bet).setPlayer(playStore.get("Ruksad"))
        def preWinnings= ((CurrentBet)bet).getWinnings();
        when:
        def currentBet = ((RouletteManagerImpl) rouletteManagerImpl).rewardPlayersForBetAndUpdatePalerInfo(
                bet,   10, CurrentBet.CurrentBetType.NUMBER,((CurrentBet)bet).getPlayer()
        )
        then:
        ((CurrentBet)bet).getOutCome().equals(CurrentBet.OutCome.WIN)

    }

    def "rewardPlayersForBetAndUpdatePalerInfo when bet playerBetTypeinfo and random number not same"(){
        given:"a bet "
        def bet=podamFactory.manufacturePojo(CurrentBet.class)
        ((CurrentBet)bet).setCurrentBetType(CurrentBet.CurrentBetType.NUMBER)
        ((CurrentBet)bet).setPlayerBetTypeInfo("11")
        ((CurrentBet)bet).setPlayer(playStore.get("Ruksad"))
        when:
        def currentBet = ((RouletteManagerImpl) rouletteManagerImpl).rewardPlayersForBetAndUpdatePalerInfo(
                bet,   10, CurrentBet.CurrentBetType.NUMBER,((CurrentBet)bet).getPlayer()
        )
        then:
        ((CurrentBet)currentBet).getOutCome().equals(CurrentBet.OutCome.LOSE)

    }

    def "rewardPlayersForBetAndUpdatePalerInfo when bet even "(){
        given:"a bet "
        def bet=podamFactory.manufacturePojo(CurrentBet.class)
        ((CurrentBet)bet).setCurrentBetType(CurrentBet.CurrentBetType.EVEN)
        ((CurrentBet)bet).setPlayerBetTypeInfo("Even")
        ((CurrentBet)bet).setPlayer(playStore.get("Ruksad"))
        def preWinnings= ((CurrentBet)bet).getWinnings();
        when:
        def currentBet = ((RouletteManagerImpl) rouletteManagerImpl).rewardPlayersForBetAndUpdatePalerInfo(
                bet,   10, CurrentBet.CurrentBetType.EVEN,((CurrentBet)bet).getPlayer()
        )
        then:
        ((CurrentBet)currentBet).getOutCome().equals(CurrentBet.OutCome.WIN)

    }

    def "rewardPlayersForBetAndUpdatePalerInfo when bet odd"(){
        given:"a bet "
        def bet=podamFactory.manufacturePojo(CurrentBet.class)
        ((CurrentBet)bet).setCurrentBetType(CurrentBet.CurrentBetType.ODD)
        ((CurrentBet)bet).setPlayerBetTypeInfo("Odd")
        ((CurrentBet)bet).setPlayer(playStore.get("Ruksad"))
        when:
        def currentBet = ((RouletteManagerImpl) rouletteManagerImpl).rewardPlayersForBetAndUpdatePalerInfo(
                bet,   10, CurrentBet.CurrentBetType.EVEN,((CurrentBet)bet).getPlayer()
        )
        then:
        ((CurrentBet)currentBet).getOutCome().equals(CurrentBet.OutCome.LOSE)

    }

}
