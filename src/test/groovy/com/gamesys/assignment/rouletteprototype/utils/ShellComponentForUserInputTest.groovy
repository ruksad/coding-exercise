package com.gamesys.assignment.rouletteprototype.utils

import com.gamesys.assignment.rouletteprototype.model.CurrentBet
import com.gamesys.assignment.rouletteprototype.model.Player
import com.gamesys.assignment.rouletteprototype.service.ConsoleServiceImpl
import com.gamesys.assignment.rouletteprototype.service.RouletteManagerImpl
import spock.lang.Specification
import uk.co.jemos.podam.api.PodamFactoryImpl

class ShellComponentForUserInputTest extends Specification {
    def consoleService = Mock(ConsoleServiceImpl)
    def rouletteManagerImpl = Mock(RouletteManagerImpl)
    def lowerBound = 1;
    def upperBound = 36
    def shellComponent;
    def podamFactory = new PodamFactoryImpl();
    def playStore = new HashMap<String,Player>();

    void setup() {
        shellComponent = new ShellComponentForUserInput(consoleService, rouletteManagerImpl, lowerBound, upperBound)
        def playerList = podamFactory.manufacturePojo(ArrayList.class,Player.class)
        ((Player)playerList.get(0)).setPlayerName("Ruksad");
        ((Player)playerList.get(1)).setPlayerName("Siddiqui");
        ((Player)playerList.get(2)).setPlayerName("Barbara");

        for(Player player:playerList){
            playStore.put(player.getPlayerName(),player)
        }
    }

    def "Testing extractConsoleInput for invalid input"() {
        given: ""
        def list = new ArrayList();
        list.add("Ruksad");
        list.add(" ")
        when: ""
        shellComponent.extractConsoleInput(list)
        then:
        thrown(Exception)
    }

    def "Testing extractConsoleInput for second arg out of range invalid input"() {
        given: ""
        def list = new ArrayList();
        list.add("Ruksad");
        list.add("100")
        list.add("9")
        rouletteManagerImpl.getPlayersStore() >> playStore
        when: ""
        shellComponent.extractConsoleInput(list)
        then:
        thrown(Exception)
    }

    def "Testing extractConsoleInput for Even odd bet"() {
        given: ""
        def list = new ArrayList();
        list.add("Ruksad");
        list.add("even")
        list.add("9")
        rouletteManagerImpl.getPlayersStore() >> playStore
        when: ""
        def res =shellComponent.extractConsoleInput(list)
        then:
        ((CurrentBet)res).getCurrentBetType().value.toLowerCase().equals(((String)list.get(1)).toLowerCase())
    }

    def "Testing extractConsoleInput for player name in not db"() {
        given: ""
        def list = new ArrayList();
        list.add("Ruksaderer");
        list.add("10")
        list.add("9")
        rouletteManagerImpl.getPlayersStore() >> playStore
        when: ""
        shellComponent.extractConsoleInput(list)
        then:
        thrown(Exception)
    }

    def "Testing extractConsoleInput for invalid number of args"() {
        given: ""
        def list = new ArrayList();
        list.add("Ruksaderer");

        rouletteManagerImpl.getPlayersStore() >> playStore
        when: ""
        shellComponent.extractConsoleInput(list)
        then:
        thrown(Exception)
    }

    def "Testing extractConsoleInput Stopping applicaiton"() {
        given: ""
        def list = new ArrayList();
        list.add("stop");
        list.add("10")
        list.add("9")

        rouletteManagerImpl.getPlayersStore() >> playStore
        when: ""
        shellComponent.extractConsoleInput(list)
        then:
        thrown(Exception)
    }

    def "Testing mapStringsToPlayer for getting the current bet"() {
        given: "input"
        def list = new ArrayList();
        list.add("Ruksad");
        list.add("10")
        list.add("9")

        rouletteManagerImpl.getPlayersStore() >> playStore
        when: " invoked mapStringsToPlayer"
        def res=shellComponent.mapStringsToPlayer(list)
        then:"throws no exception"
        noExceptionThrown()
        ((CurrentBet)res).getPlayer().getPlayerName().equals(list.get(0))
        BigDecimal.isInstance(((CurrentBet)res).getChipDenomination())
    }
}
