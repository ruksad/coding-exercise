package com.gamesys.assignment.rouletteprototype.utils

import com.gamesys.assignment.rouletteprototype.model.CurrentBet
import org.springframework.core.io.ClassPathResource
import spock.lang.Specification

class AppUtilsTest extends Specification {

    def "LoadStoreFromFile for well formed csv"() {
        given: "file location"
        ClassPathResource filePath = new ClassPathResource("store.csv")
        AppUtils appUtils = new AppUtils();
        when: "method is invoked"
        def res = appUtils.loadStoreFromFile(filePath.getFile().getPath())
        then: "no exception and returned list has size"
        Objects.nonNull(res)
    }

    def "LoadStoreFromFile for not well formed csv"() {
        given: "file location"
        ClassPathResource filePath = new ClassPathResource("storeWithnonValues.csv")
        AppUtils appUtils = new AppUtils();
        when: "method is invoked"
        def res = appUtils.loadStoreFromFile(filePath.getFile().getPath())
        then: "no exception and returned list has size"
        Objects.nonNull(res)
    }

    def "LoadStoreFromFile for only with playerNames"() {
        given: "file location"
        ClassPathResource filePath = new ClassPathResource("TestWithNoHistory.txt")
        AppUtils appUtils = new AppUtils();
        when: "method is invoked"
        def res = appUtils.loadStoreFromFile(filePath.getFile().getPath())
        then: "no exception and returned list has size"
        (res.get(0).getTotalBet().compareTo(BigDecimal.valueOf(0.0)) == 0)
    }

    def "LoadPlayersFromCSV"() {
    }

    def "checkIfBigDecimalIsInRageOfBounds when upper and lower and bigdecimal are same"() {
        given:
        BigDecimal bigDecimal = new BigDecimal(5)
        def lowerBound = 5;
        def upperBound = 5;

        when:
        def booleanValue = AppUtils.checkIfBigDecimalIsInRageOfBounds(bigDecimal, lowerBound, upperBound)

        then:
        (booleanValue)
    }

    def "checkIfBigDecimalIsInRageOfBounds when number is not bounds"() {
        given:
        BigDecimal bigDecimal = new BigDecimal(15)
        def lowerBound = 5;
        def upperBound = 5;

        when:
        def booleanValue = AppUtils.checkIfBigDecimalIsInRageOfBounds(bigDecimal, lowerBound, upperBound)

        then:
        (!booleanValue)
    }

    def "getPlayerBetTypeForNumber when given"() {
        given:
        def number = new Random().nextInt();

        when:
        def currentBetType = AppUtils.getPlayerBetTypeForNumber(number)

        then:
        (((CurrentBet.CurrentBetType) currentBetType).equals(CurrentBet.CurrentBetType.EVEN) ||
                ((CurrentBet.CurrentBetType) currentBetType).equals(CurrentBet.CurrentBetType.ODD))
    }
}
