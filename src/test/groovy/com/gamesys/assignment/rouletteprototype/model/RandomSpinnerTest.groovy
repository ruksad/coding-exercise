package com.gamesys.assignment.rouletteprototype.model

import spock.lang.Specification

import java.util.stream.Collectors

class RandomSpinnerTest extends Specification {
    def lowerBound = 10
    def upperBound = 20;
    def randomSpinner = new RandomSpinner(lowerBound, upperBound)

    def "GenerateSpinNumber"() {
        given:

        when:
        def res = randomSpinner.generateSpinNumber();
        then:
        Objects.nonNull(res)
    }

    def "GenerateSpinNumber check if number in range"() {
        given:

        when:
        List<Integer> ints = new ArrayList<Integer>()
        for (int i = 0; i < 100; i++) {
            def res = randomSpinner.generateSpinNumber();
            ints.add(res)
        }

        then:

        List<Integer> intsOutOfBound = ints.stream().filter({x->x>lowerBound && x<lowerBound}) .collect ( Collectors.toList ( ) )
        ( intsOutOfBound.isEmpty ( ) )

    }
}
