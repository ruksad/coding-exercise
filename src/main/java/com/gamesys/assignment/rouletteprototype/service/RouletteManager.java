package com.gamesys.assignment.rouletteprototype.service;

import org.springframework.stereotype.Service;

@Service
public interface RouletteManager {

  boolean loadStore();

  void scheduleBetConsumer();

  boolean stopBetConsumer();
}
