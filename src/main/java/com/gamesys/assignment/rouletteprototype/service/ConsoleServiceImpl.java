package com.gamesys.assignment.rouletteprototype.service;

import com.gamesys.assignment.rouletteprototype.model.CurrentBet;
import com.gamesys.assignment.rouletteprototype.model.Player;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConsoleServiceImpl implements ConsoleService {

  PrintStream printStream = System.out;
  InputStream inputStream = System.in;
  public static final String GREET_MESSAGE = "Please enter the PlayerName bet(EVEN,ODD,NUMBER) betDenomination and hit enter";
  public static final String BET_ACCEPTED = "Your bet is Accepted";
  public static final String EMPTY_STRING = "";
  private final BufferedReader bufferedReader;

  @Autowired
  ConsoleServiceImpl() {
    this.bufferedReader = new BufferedReader(new InputStreamReader(this.inputStream));
  }

  @Override
  public List<String> readFromConsole() throws IOException {
    String s = this.bufferedReader.readLine().toLowerCase();
    return Arrays.asList(s.split(" "));
  }

  @Override
  public void writeToConsole(String msg, String... args) {
    this.printStream.print("> ");
    this.printStream.printf(msg, (Object[]) args);
    this.printStream.println("");
  }

  @Override
  public void printBetResults(int randomNumber, List<CurrentBet> currentBets) {
    this.printStream.println("Number: " + randomNumber);
    this.printStream
        .println(String.format("%10s %10s %10s %10s", "Player", "Bet", "Outcome", "Winnings"));
    this.printStream.println("-----------------------------------------");
    currentBets.forEach(currentBet ->
        this.printStream.println(String
            .format("%10s %10s %10s %10s", currentBet.getPlayer().getPlayerName(),
                currentBet.getPlayerBetTypeInfo(), currentBet.getOutCome().getValue(),
                currentBet.getWinnings()))
    );
    this.printStream.println();
  }

  @Override
  public void printTotalWinTotalBet(Map<String, Player> map) {
    this.printStream
        .println(String.format("%10s %10s %10s", "Player", "Total win", "Total Bet"));
    this.printStream.println("-----------------------------------------");
    map.forEach((key, value) ->
        this.printStream
            .println(String.format("%10s %10s %10s", value.getPlayerName(), value.getTotalWin(),
                value.getTotalBet()))
    );
  }
}
