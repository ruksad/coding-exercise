package com.gamesys.assignment.rouletteprototype.service;

import com.gamesys.assignment.rouletteprototype.exceptions.RouletteGenericException;
import com.gamesys.assignment.rouletteprototype.model.CurrentBet;
import com.gamesys.assignment.rouletteprototype.model.CurrentBet.CurrentBetType;
import com.gamesys.assignment.rouletteprototype.model.CurrentBet.OutCome;
import com.gamesys.assignment.rouletteprototype.model.Player;
import com.gamesys.assignment.rouletteprototype.model.RandomSpinner;
import com.gamesys.assignment.rouletteprototype.utils.AppUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Getter
@Setter
@Slf4j
public class RouletteManagerImpl implements RouletteManager {

  private String loadStoreFile;
  //bounded queue for bets so the bets are not piled
  private BlockingQueue blockingQueueForBets = new ArrayBlockingQueue<CurrentBet>(100);
  private Map<String, Player> playersStore = new ConcurrentHashMap<>(4);
  private final ScheduledExecutorService executorService;
  private final long spinnerTime;

  private final Runnable runnable = () -> consumeBetAndRewardPlayerForSpin();

  private final RandomSpinner randomSpinner;
  private final int evenReward;
  private final int oddReward;
  private final int numberReward;
  private final ConsoleService consoleService;

  private ScheduledFuture<?> scheduledFuture;

  @Autowired
  RouletteManagerImpl(@Value("${roulette.prototype.db-load-file}") String loadStoreFile,
      @Value("${roulette.prototype.spinner.schedule-time-in-mins}") long spinnerTime,
      @Value("${roulette.prototype.reward.even}") int evenReward,
      @Value("${roulette.prototype.reward.odd}") int oddReward,
      @Value("${roulette.prototype.reward.number}") int numberReward,
      ScheduledExecutorService scheduledExecutorService, RandomSpinner randomSpinner,
      ConsoleService consoleService) {
    this.loadStoreFile = loadStoreFile;
    this.executorService = scheduledExecutorService;
    this.spinnerTime = spinnerTime;
    this.randomSpinner = randomSpinner;
    this.evenReward = evenReward;
    this.oddReward = oddReward;
    this.numberReward = numberReward;
    this.consoleService = consoleService;
  }

  @Override
  public boolean loadStore() {
    List<Player> players;
    try {
      players = AppUtils.loadStoreFromFile(loadStoreFile);
    } catch (Exception e) {
      throw new RouletteGenericException(e.getMessage());
    }
    Map<String, Player> collect = players.stream()
        .collect(
            Collectors.toMap(player -> player.getPlayerName().toLowerCase(), player -> player));
    playersStore.putAll(collect);
    return !collect.isEmpty();
  }

  @Override
  public void scheduleBetConsumer() {
    scheduledFuture = executorService
        .scheduleWithFixedDelay(runnable, spinnerTime, spinnerTime, TimeUnit.SECONDS);
  }

  public boolean stopBetConsumer() {
    return this.scheduledFuture.cancel(true);
  }

  private void consumeBetAndRewardPlayerForSpin() {

    if (!this.blockingQueueForBets.isEmpty()) {
      int randomNumber = this.randomSpinner.generateSpinNumber();
      ArrayList<CurrentBet> currentBets = new ArrayList<>(4);
      blockingQueueForBets.drainTo(currentBets);
      currentBets.forEach(currentBet -> {
        CurrentBet populatedCurrentBet = evaluateBetForPlayer(currentBet, randomNumber);
        playersStore.put(populatedCurrentBet.getPlayer().getPlayerName().toLowerCase(),
            populatedCurrentBet.getPlayer());
      });
      this.consoleService.printBetResults(randomNumber, currentBets);
      this.consoleService.printTotalWinTotalBet(playersStore);
    } else {
      consoleService.writeToConsole("Please enter a bet to play....");
    }
  }


  private CurrentBet evaluateBetForPlayer(CurrentBet currentBet, int randomNumber) {

    CurrentBetType currentBetType = AppUtils.getPlayerBetTypeForNumber(randomNumber);
    Player player = currentBet.getPlayer();
    player.setTotalBet(player.getTotalBet().add(currentBet.getChipDenomination()));
    return rewardPlayersForBetAndUpdatePalerInfo(currentBet, randomNumber,
        currentBetType, player);
  }

  private CurrentBet rewardPlayersForBetAndUpdatePalerInfo(CurrentBet currentBet, int randomNumber,
      CurrentBetType currentBetType, Player player) {
    switch (currentBet.getCurrentBetType()) {
      case ODD:
        if (currentBetType.equals(CurrentBetType.ODD)) {
          currentBet.setOutCome(OutCome.WIN);
          currentBet
              .setWinnings(
                  AppUtils.multiplyBigDecimalWithInt(currentBet.getChipDenomination(), oddReward));
          player.setTotalWin(player.getTotalWin().add(currentBet.getWinnings()));
        } else {
          currentBet.setOutCome(OutCome.LOSE);
          currentBet
              .setWinnings(BigDecimal.valueOf(0.0));
        }
        break;

      case EVEN:
        if (currentBetType.equals(CurrentBetType.EVEN)) {
          currentBet.setOutCome(OutCome.WIN);
          currentBet
              .setWinnings(
                  AppUtils.multiplyBigDecimalWithInt(currentBet.getChipDenomination(), evenReward));
          player.setTotalWin(player.getTotalWin().add(currentBet.getWinnings()));
        } else {
          currentBet.setOutCome(OutCome.LOSE);
          currentBet
              .setWinnings(BigDecimal.valueOf(0.0));
        }
        break;

      case NUMBER:
        /**
         * checking if user given bet type is equal to random number
         */
        boolean b = AppUtils.checkIfBigDecimalIsInRageOfBounds(
            BigDecimal.valueOf(Integer.valueOf(currentBet.getPlayerBetTypeInfo())), randomNumber,
            randomNumber);
        if (b) {
          currentBet.setOutCome(OutCome.WIN);
          currentBet
              .setWinnings(AppUtils
                  .multiplyBigDecimalWithInt(currentBet.getChipDenomination(), numberReward));
          player.setTotalWin(player.getTotalWin().add(currentBet.getWinnings()));
        } else {
          currentBet.setOutCome(OutCome.LOSE);
          currentBet
              .setWinnings(BigDecimal.valueOf(0.0));
        }


    }
    return currentBet;
  }


}
