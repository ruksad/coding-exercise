package com.gamesys.assignment.rouletteprototype.service;

import com.gamesys.assignment.rouletteprototype.model.CurrentBet;
import com.gamesys.assignment.rouletteprototype.model.Player;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ConsoleService {


  List<String> readFromConsole() throws IOException;
  void writeToConsole(String msg,String...args);
  void printBetResults(int randomNumber, List<CurrentBet> currentBets);
  void printTotalWinTotalBet(Map<String,Player> map);
}
