package com.gamesys.assignment.rouletteprototype.exceptions;

public class RouletteGenericException extends RuntimeException {
  public RouletteGenericException(String message){
    super(message);
  }
}
