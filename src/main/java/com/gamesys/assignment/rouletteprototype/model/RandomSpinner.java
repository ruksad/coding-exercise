package com.gamesys.assignment.rouletteprototype.model;

import java.util.concurrent.ThreadLocalRandom;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@EqualsAndHashCode
@Getter
public class RandomSpinner {

  private final int lowerBound;
  private final int upperBound;


  @Autowired
  RandomSpinner(@Value("${game.prototype.randomNumber.lowerBound:0}") int lowerBound,
      @Value("${game.prototype.randomNumber.upperBound:36}") int upperBound) {
    this.lowerBound = lowerBound;
    this.upperBound = upperBound;
  }

  /**
   * method will return number between the configured range both bound numbers inclusive i.e if we
   * give range 10 to 90 this function will generate numbers between 10(inclusive) and
   * 90(inclusive)
   */
  public int generateSpinNumber() {
    return ThreadLocalRandom.current().nextInt(lowerBound, upperBound + 1);
  }
}
