package com.gamesys.assignment.rouletteprototype.model;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder(toBuilder = true)
@EqualsAndHashCode
@ToString
public class CurrentBet {

  public enum CurrentBetType {
    EVEN("even"),
    ODD("odd"),
    NUMBER("number");
    @Getter
    private final String value;

    CurrentBetType(String value) {
      this.value = value;
    }

    public static boolean checkValueExist(String value) {
      boolean flag = false;
      for (CurrentBetType status : values()) {
        if (status.value.equals(value)) {
          flag = true;
        }
      }
      return flag;
    }
  }

  public enum OutCome {
    LOSE("LOSE"),
    WIN("WIN");
    @Getter
    private final String value;

    OutCome(String value) {
      this.value = value;
    }

    public static boolean checkValueExist(String value) {
      boolean flag = false;
      for (OutCome status : values()) {
        if (status.value.equals(value)) {
          flag = true;
        }
      }
      return flag;
    }
  }

  private Player player;
  private CurrentBetType currentBetType;
  private BigDecimal chipDenomination = BigDecimal.valueOf(0.0);
  /*
    below fields will form display view
   */
  private String playerBetTypeInfo;
  private BigDecimal winnings=BigDecimal.valueOf(0.0);
  private OutCome outCome;
}
