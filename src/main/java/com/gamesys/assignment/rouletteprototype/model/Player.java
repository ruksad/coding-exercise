package com.gamesys.assignment.rouletteprototype.model;

import com.opencsv.bean.CsvBindByName;
import java.math.BigDecimal;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Player {

  @CsvBindByName(column = "player_name")
  private String playerName;
  @EqualsAndHashCode.Exclude
  @CsvBindByName(column = "total_win")
  private BigDecimal totalWin = BigDecimal.valueOf(0.0);
  @EqualsAndHashCode.Exclude
  @CsvBindByName(column = "total_bet")
  private BigDecimal totalBet = BigDecimal.valueOf(0.0);
}
