package com.gamesys.assignment.rouletteprototype.config;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouletteConfig {
  @Bean
  public ScheduledExecutorService executorService() {
    return new ScheduledThreadPoolExecutor(6);
  }

}

