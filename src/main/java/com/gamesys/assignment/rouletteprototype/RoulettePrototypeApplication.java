package com.gamesys.assignment.rouletteprototype;

import lombok.extern.slf4j.Slf4j;
import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.shell.jline.PromptProvider;

@SpringBootApplication
@Slf4j
public class RoulettePrototypeApplication {

  public static void main(String[] args) {
    SpringApplication.run(RoulettePrototypeApplication.class, args);
  }

  @Bean
  public PromptProvider myPromptProvider() {
    return () -> new AttributedString("Roulette:>",
        AttributedStyle.DEFAULT.foreground(AttributedStyle.YELLOW));
  }

}
