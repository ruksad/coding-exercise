package com.gamesys.assignment.rouletteprototype.utils;

import com.gamesys.assignment.rouletteprototype.model.CurrentBet.CurrentBetType;
import com.gamesys.assignment.rouletteprototype.model.Player;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.exceptions.CsvException;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class AppUtils {

  private AppUtils() {
  }

  public static List<Player> loadStoreFromFile(String fileName) throws CsvException, IOException {
    List<Player> players;
    try (Reader reader = Files.newBufferedReader(Paths.get(fileName))) {
      CsvToBean<Player> playerCsvToBean = new CsvToBean<>();
      players = loadPlayersFromCSV(reader, playerCsvToBean);
    }
    return players;
  }

  public static List<Player> loadPlayersFromCSV(Reader reader, CsvToBean csvToBean)
      throws CsvException {
    HeaderColumnNameMappingStrategy<Player> playerHeaderColumnNameMappingStrategy = new HeaderColumnNameMappingStrategy<>();
    playerHeaderColumnNameMappingStrategy.setType(Player.class);
    return csvToBean.parse(playerHeaderColumnNameMappingStrategy, reader, true);
  }

  public static BigDecimal convertStringToBigDecimal(String val) {
    return BigDecimal.valueOf(Double.valueOf(val));
  }

  public static boolean checkIfBigDecimalIsInRageOfBounds(BigDecimal bigDecimal, int lowerBound,
      int upperBound) {
    return bigDecimal.compareTo(BigDecimal.valueOf(lowerBound)) >= 0 && bigDecimal
        .compareTo(BigDecimal.valueOf(upperBound)) <= 0;
  }

  public static CurrentBetType getPlayerBetTypeForNumber(int number) {
    if (number % 2 == 0) {
      return CurrentBetType.EVEN;
    } else if (number % 2 != 0) {
      return CurrentBetType.ODD;
    }
    return CurrentBetType.NUMBER;
  }

  public static BigDecimal multiplyBigDecimalWithInt(BigDecimal bigDecimal, int value) {
    return bigDecimal.multiply(BigDecimal.valueOf(value));
  }
}
