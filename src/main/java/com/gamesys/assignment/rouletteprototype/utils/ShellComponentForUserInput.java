package com.gamesys.assignment.rouletteprototype.utils;

import com.gamesys.assignment.rouletteprototype.exceptions.RouletteGenericException;
import com.gamesys.assignment.rouletteprototype.model.CurrentBet;
import com.gamesys.assignment.rouletteprototype.model.CurrentBet.CurrentBetBuilder;
import com.gamesys.assignment.rouletteprototype.model.CurrentBet.CurrentBetType;
import com.gamesys.assignment.rouletteprototype.model.Player;
import com.gamesys.assignment.rouletteprototype.service.ConsoleService;
import com.gamesys.assignment.rouletteprototype.service.ConsoleServiceImpl;
import com.gamesys.assignment.rouletteprototype.service.RouletteManagerImpl;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
@Slf4j
@Getter
public class ShellComponentForUserInput {

  public enum AppStatus {
    START("start"), STOP("stop");
    private String value;

    AppStatus(String value) {
      this.value = value;
    }
  }

  private AppStatus appStatus;

  private final ConsoleService consoleService;
  private final RouletteManagerImpl rouletteManager;
  private final int lowerBound;
  private final int upperBound;

  @Autowired
  ShellComponentForUserInput(ConsoleService consoleService,
      RouletteManagerImpl rouletteManager,
      @Value("${roulette.prototype.randomNumber.lowerBound:0}") int lowerBound,
      @Value("${roulette.prototype.randomNumber.upperBound:36}") int upperBound) {
    this.consoleService = consoleService;
    this.rouletteManager = rouletteManager;
    this.lowerBound = lowerBound;
    this.upperBound = upperBound;

  }

  @ShellMethod("input")
  public void start() {
    this.appStatus = AppStatus.START;
    if (rouletteManager.loadStore()) {
      this.rouletteManager.scheduleBetConsumer();
      printGreetMessage("");

      while (this.appStatus.equals(AppStatus.START)) {
        try {
          List<String> strings = this.consoleService.readFromConsole();
          if (strings.get(0).equals(ConsoleServiceImpl.EMPTY_STRING)) {
            continue;
          }

          addBetToQueue(strings);
        } catch (RouletteGenericException e) {
          printGreetMessage(e.getMessage());
        } catch (Exception e) {
          throw new RouletteGenericException(e.getMessage());
        }
      }

    } else {
      throw new RouletteGenericException("User record store seems to be empty");
    }
  }

  private void addBetToQueue(List<String> strings) {
    try {
      rouletteManager.getBlockingQueueForBets().put(extractConsoleInput(strings));
    } catch (InterruptedException e) {
      this.appStatus = AppStatus.STOP;
      Thread.currentThread().interrupt();
    }
  }

  private CurrentBet extractConsoleInput(List<String> strings) {
    if (strings.get(0).equals(AppStatus.STOP.value)) {
      this.appStatus = AppStatus.STOP;
      this.rouletteManager.stopBetConsumer();
      throw new RouletteGenericException(
          "Bye bye see you next time and remember to enter message format like ");
    }
    return mapStringsToPlayer(strings);
  }

  private CurrentBet mapStringsToPlayer(List<String> bet) {
    if (bet.size() != 3) {
      throw new RouletteGenericException("please provide valid input");
    }
    Map<String, Player> playersStore = rouletteManager.getPlayersStore();

    if (!playersStore.containsKey(bet.get(0))) {
      throw new RouletteGenericException("please provide user which exists in store file");
    }
    return extractValuesFromUserInput(bet, playersStore);
  }

  private CurrentBet extractValuesFromUserInput(List<String> bet,
      Map<String, Player> playersStore) {
    CurrentBetBuilder builder = CurrentBet.builder();
    builder.player(playersStore.get(bet.get(0)));

    BigDecimal denomination;

    try {
      denomination = AppUtils.convertStringToBigDecimal(bet.get(2));
      builder.chipDenomination(denomination);
    } catch (NumberFormatException e) {
      throw new RouletteGenericException("Chip denomination for bet is invalid " + e.getMessage());
    }

    String s = bet.get(1);
    try {
      if (CurrentBetType.checkValueExist(s)) {
        if (CurrentBetType.EVEN.getValue().equals(s)) {
          builder.currentBetType(CurrentBetType.EVEN);
        } else if (CurrentBetType.ODD.getValue().equals(s)) {
          builder.currentBetType(CurrentBetType.ODD);
        }
      } else {
        BigDecimal bigDecimal = AppUtils.convertStringToBigDecimal(s);
        if (AppUtils.checkIfBigDecimalIsInRageOfBounds(bigDecimal, lowerBound, upperBound)) {
          builder.currentBetType(CurrentBetType.NUMBER);
        } else {
          throw new RouletteGenericException(
              "Bet is invalid please provide valid bet between " + lowerBound + "and inclusive "
                  + upperBound);
        }
      }
      builder.playerBetTypeInfo(s);
    } catch (NumberFormatException e) {
      throw new RouletteGenericException("Bet type is invalid " + e.getMessage());
    }
    CurrentBet build = builder.build();
    this.consoleService.writeToConsole("\n" + ConsoleServiceImpl.BET_ACCEPTED);
    return build;
  }

  public void printGreetMessage(String msg) {
    this.consoleService.writeToConsole(msg + "\n" + ConsoleServiceImpl.GREET_MESSAGE);
  }
}
